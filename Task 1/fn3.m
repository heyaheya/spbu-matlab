function dydx = fn3(x,y)
dydx = y^(1/3);
end