function dydx = fn2(x,y)
dydx = y^2 + 1;
end