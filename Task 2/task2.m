close all
b = 0.01;
h = 0.0001;

task2_1(b,h)
% task2_2(b,h)
% task2_3(b,h)
% task2_4(b,h)

function task2_1(b,h)
    a = 0;
    y0 = [1; -4.1];
    x = linspace(a,b,ceil(b/h));
    yy = @(x,y) [y(2); 16.81*y(1)];
    y1 = Euler(x,yy,y0);
    y11 = Runge_Kutta(x,yy,y0);
    figure('Color','w')
    plot(x,exp(-4.1*x),'g')
    figure('Color','w')
    plot(x,y1(1,:),'r',x,y11(1,:),'b')
    yy = @(x,y) [y(2); -8.2*y(2) - 16.81*y(1)];
    y2 = Euler(x,yy,y0);
    y22 = Runge_Kutta(x,yy,y0);
    figure('Color','w')
    plot(x,y2(1,:),'r',x,y22(1,:),'b')
end

function task2_2(b,h)
    a = 0;
    v = 1;
    y0 = [0; 1];
    x = linspace(a,b,ceil(b/h));
    yy = @(x,y) [y(2); -v*y(2)-sin(y(1))];
    % yy = @(x,y) [y(2); -v*y(2)-y(1)];
    y1 = Euler(x,yy,y0);
    y11 = Runge_Kutta(x,yy,y0);
    figure('Color','w')
    plot(x,y1(1,:),'b',x,y11(1,:),'r')
end

function task2_3(b,h)
    a = 0;
    y0 = 1;
    x = linspace(a,b,ceil(b/h));
    yy = @(x,y) -10000*y;
    y1 = Euler(x,yy,y0);
    y11 = implicitEuler(x,yy,y0); 
    figure('Color','w')
    plot(x, y1(1,:), 'r', x,y11(1,:),'g',x,exp(-10000*x),'b')
end

function task2_4(b,h)
    a = 0;
    y0 = [1; 1];
    x = linspace(a,b,ceil(b/h));
    yy = @(x,y) [y(2); -100000.001*y(2)-100*y(1)];
    %y1 = Euler(x,yy,y0);  
    y11 = implicitEuler(x, yy, y0);
    %y111 = Runge_Kutta(x,yy,y0);
    figure('Color','w')
    plot(x,y11(1,:),'g')
end

