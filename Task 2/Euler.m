function y=Euler(x,yy,y0)
    h = x(2)-x(1);
    y = zeros(length(y0),length(x));
    y(:,1) = y0;
    for n=2:length(x)
        y(:,n) = y(:,n-1) + h*yy(x(n-1),y(:,n-1));
    end
end